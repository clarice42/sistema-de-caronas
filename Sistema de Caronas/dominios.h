#ifndef DOMINIOS_H_INCLUDED
#define DOMINIOS_H_INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>

using namespace std;


// Definição da classe para o codigo de caronas
/// \brief Declaracao de classe de dominio CodigoCarona
///
/// \param codigo
/// \return nao faz retorno
///
///
class CodigoCarona {

private:
    ///Declaração da variavel codigo no metodo privado da classe
    string codigo;
    ///validação do codigo como string
    void validar(string);

public:
    ///Define o tamanho maximo do codigo de carona
    const static int TAMANHO_CODIGO_CARONA = 4;

    ///Metodo Set para o Dominio de Codigo de Carona
    /// \param string
    void setCodigo(string);

    ///Metodo Get para o Dominio de Codigo de Carona
    /// \return codigo
    string getCodigo() const {
        return codigo;
    }
};


/// \brief Declaracao da classe de dominio Cidade
///
/// \param cidade
/// \return nao faz retorno
///
///
class Cidade {

// Métodos privados
private:
    ///Declaração da variavel cidade no metodo privado da classe
    string cidade;
    ///Validação do dominio Cidade como string
    void validar(string);

// Métodos públicos
public:
    ///Define o Tamanho maximo de caracteres para o dominio Cidade
    const static int TAMANHO_MAX_CARACTERES = 10;
    ///Define o tamanho minimo de caracteres para o dominio Cidade
    const static int TAMANHO_MIN_CARACTERES = 1;

    ///Metodo Set para o dominio Cidade
    /// \param string
    void setCidade(string);
    ///Metodo Get para o dominio Cidade
    /// \return cidade
    string getCidade() const {
        return cidade;
    }
};



/// \brief Declaracao da classe de dominio Estado
///
/// \param estado
/// \return nao faz retorno
///
///
class Estado {

// Métodos privados
private:
    ///Declaração da variavel estado no metodo privado da classe
    string estado;
    ///Validação do dominio Estado como string
    void validar(string);

// Métodos públicos
public:

    ///Metodo Set para o dominio Estado
    /// \param string
    void setEstado(string);
    ///Metodo Get para o dominio Estado
    /// \return estado
    string getEstado() const {
        return estado;
    }
};


/// \brief Declaracao da classe de dominio Data
///
/// \param data
/// \return nao faz retorno
///
///
class Data {

// Métodos privados
private:
    ///Declaração da variavel data no metodo privado da classe
    string data;
    ///Validação do dominio Data como string
    void validar(string);

// Métodos públicos
public:

    ///Metodo Set para o dominio Data
    /// \param string
    void setData(string);
    ///Metodo Get para o dominio Data
    /// \return data
    string getData() const {
        return data;
    }
};



/// \brief Declaracao da classe de dominio Duracao
///
/// \param duracao
/// \return
///
///
class Duracao {

// Métodos privados
private:
    ///Declaração da variavel duração no metodo privado
    int duracao;
    ///Faz a validação da variavel validar como inteiro
    void validar(int);

// Métodos públicos
public:
    ///Define a duração minima da viagem
    const static int DURACAO_MIN = 1;
    ///Define a duração maxima de viagem
    const static int DURACAO_MAX = 48;

    // Setter e getter
    ///Metodo Setter para o dominio duracao
    /// \param int
    void setDuracao(int);
    ///Metodo Getter para o dominio duracao
    /// \return duracao
    int getDuracao() const {
        return duracao;
    }
};


/// \brief Declaracao da classe de dominio Vagas
///
/// \param vagas
/// \param
/// \return nao faz retorno
///
///
class Vagas {

// Métodos privados
private:
    ///Declara a variavel vaga como inteiro
    int vagas;
    ///Faz a validacao da variavel como inteiro
    void validar(int);

// Métodos públicos
public:
    ///Define a quantidade minima de vagas
    const static int VAGAS_MIN = 0;
    ///Define a quantidade maxima de vagas
    const static int VAGAS_MAX = 4;

    // Setter e getter
    ///Metodo Setter para o dominio Vagas
    /// \param int
    void setVagas(int);
    ///Metodo Getter para o dominio Vagas
    /// \return vagas
    int getVagas() const {
        return vagas;
    }
};


/// \brief Declaracao da classe de dominio Preco
///
/// \param preco
/// \return nao faz retorno
///
///
class Preco {

// Métodos privados
private:
    ///Declara a variavel preco no metodo privado
    float preco;
    ///Faz a validacao da variavel preco como float
    void validar(float);

// Métodos públicos
public:
    ///Define o preco minimo pela carona
    constexpr static float PRECO_MIN = 1.00;
    ///Define o preco maximo pela carona
    constexpr static float PRECO_MAX = 5000.00;

    // Setter e getter
    ///Metodo Setter para o dominio Preco
    /// \param float
    void setPreco(float);
    ///Metodo Getter para o dominio Preco
    /// \return preco
    float getPreco() const {
        return preco;
    }
};



/// \brief Declaracao da classe de dominio CodigoReserva
///
/// \param codigo_reserva
/// \return nao faz retorno
///
///
class CodigoReserva {

// Métodos privados
private:
    ///Declara a variavel codigo_reserva no metodo privado
    string codigo_reserva;
    ///Faz a validacao da variavel como string
    void validar(string);

// Métodos públicos
public:
    ///Define o tamanho maximo do codigo de reserva
    const static int TAMANHO_CODIGO_RESERVA = 5;

    // Setter e getter
    ///Metodo Setter para o dominio CodigoReserva
    /// \param string
    void setCodigoReserva(string);
    ///Metodo Getter para o dominio CodigoReserva
    /// \return codigo_reserva
    string getCodigoReserva() const {
        return codigo_reserva;
    }
};


/// \brief Declaracao da classe de dominio Assento
///
/// \param assento
/// \return nao faz retorno
///
///
class Assento {

private:
    ///Declara a variavel assento no metodo privado
    char assento;
    ///Faz a validacao da variavel como char
    void validar(char);

public:
    ///Metodo Setter para o dominio Assento
    /// \param char
    void setAssento(char);
    ///Metodo Getter para o dominio Assento
    /// \return assento
    char getAssento() const {
        return assento;
    }
};



/// \brief Declaracao da classe de dominio Bagagem
///
/// \param bagagem
/// \return nao faz retorno
///
///
class Bagagem {

private:
    ///Declara a variavel bagagem no metodo privado
    int bagagem;
    ///Faz a validacao da variavel como inteiro
    void validar(int);

public:
    ///Define o numero minimo de bagagens
    const static int VALOR_MINIMO = 0;
    ///Define o numero maximo de bagagens
    const static int VALOR_MAXIMO = 4;

    ///Metodo Setter para o dominio Bagagem
    /// \param int
    void setBagagem(int);
    ///Metodo Getter para o dominio Bagagem
    /// \return bagagem
    int getBagagem() const {
        return bagagem;
    }
};



/// \brief Declaracao da classe de dominio Banco
///
/// \param banco
/// \return nao faz retorno
///
///
class Banco {

private:
    ///Declara a variavel banco no metodo privado
    string banco;
    ///Faz a validacao da variavel como string
    void validar(string);

public:
    ///Define o tamanho maximo do codigo do banco
    const static int TAMANHO_CODIGO_BANCO = 3;

    ///Metodo Setter para o dominio Banco
    /// \param string
    void setBanco(string);
    ///Metodo Getter para o dominio Banco
    /// \return banco
    string getBanco() const {
        return banco;
    }
};



/// \brief Declaracao da classe de dominio Agencia
///
/// \param agencia
/// \return nao faz retorno
///
///
class Agencia {

private:
    ///Declara a variavel agencia no metodo privado
    string agencia;
    ///Faz a validação da variavel como string
    void validar(string);

public:
    ///Defino tamanho maximo do numero da agencia referente ao banco
    const static int TAMANHO_MAX = 5;

    ///Metodo Setter para o dominio Agencia
    /// \param string
    void setAgencia(string);
    ///Metodo Getter para o dominio Agencia
    /// \return agencia
    string getAgencia() const {
        return agencia;
    }
};


/// \brief Declaracao da classe de dominio NumeroConta
///
/// \param numero
/// \return nao faz retorno
///
///
class NumeroConta {

private:
    ///Declara a variavel numero
    string numero;
    ///Faz a validacao da variavel como string
    void validar(string);

public:
    ///Define o tamanho maximo do numero da conta
    const static int TAMANHO_MAX = 7;

    ///Metodo Setter para o dominio NumeroConta
    /// \param string
    void setNumeroConta(string);
    ///Metodo Getter parao dominio NumeroConta
    /// \return numero
    string getNumeroConta() const {
        return numero;
    }
};



/// \brief Declaracao da classe de dominio Nome
///
/// \param nome
/// \return nao faz retorno
///
///
class Nome {

private:
    ///Declara a variavel nome
    string nome;
    ///Faz a validacao da variavel como string
    void validar(string);

public:
    ///Define o minimo de caracteres para o nome do usuario
    const static int TAMANHO_MIN_CARACTERES = 1;
    ///Define o maximo de caracteres para o nome do usuario
    const static int TAMANHO_MAX_CARACTERES = 20;

    ///Metodo Setter para o dominio Nome
    ///\param string
    void setNome(string);
    ///Metodo Getter para o dominio Nome
    ///\return nome
    string getNome() const {
        return nome;
    }
};



/// \brief Declaracao da classe de dominio Email
///
/// \param email
/// \return nao faz retorno
///
///
class Email{

private:
    ///Declara a variavel email
    string email;
    ///Valida a variavel como string
    void validar(string);

public:
    ///Metodo Setter para o dominio Email
    ///\param string
    void setEmail(string);
    ///Metodo Getter para o dominio Email
    ///\return email
    string getEmail() const{
        return email;
    }
};



/// \brief Declaracao da classe de dominio Senha
///
/// \param senha
/// \return nao faz retorno
///
///
class Senha{

private:
    ///Declara a variavel senha
    string senha;
    ///Faz a validacao da variavel como string
    void validar(string);

public:
    ///Define o tamanho maximo da senha
    const static int TAMANHO = 5;

    ///Metodo Setter para o dominio Senha
    ///\param string
    void setSenha(string);
    ///Metodo Getter para o dominio Senha
    ///\return senha
    string getSenha() const{
        return senha;
    }
};



/// \brief Declaracao da classe de dominio Cpf
///
/// \param cpf
/// \return nao faz retorno
///
///
class Cpf{

private:
    ///Declara a variavel cpf
    string cpf;
    ///Faz a validacao da variavel como string
    void validar(string);

public:
    ///Define o tamanho maximo do cpf
    const static int TAMANHO = 11;

    ///Metodo Setter para o dominio Cpf
    ///\param string
    void setCpf(string);
    ///Metodo Getter para o dominio Cpf
    ///\return cpf
    string getCpf() const{
        return cpf;
    }
};



/// \brief Declaracao da clase de dominio Telefone
///
/// \param telefone
/// \return nao faz retorno
///
///
class Telefone {

private:
    ///Declara a variavel telefone
    string telefone;
    ///Faz a validacao da variavel como string
    void validar(string);

public:
    ///Metodo Setter para o dominio Telefone
    ///\param string
    void setTelefone(string);
    ///Metodo Getter para o dominio Telefone
    ///\return telefone
    string getTelefone() const{
        return telefone;
    }
};

#endif
