#ifndef TESTESDOMINIO_H_INCLUDED
#define TESTESDOMINIO_H_INCLUDED

#include "dominios.h"

using namespace std;

// Teste para o dominio codigo de carona
class TUCodigoCarona {

private:

    string VALOR_VALIDO = "1947";
    string VALOR_INVALIDO = "11235";

    // Referencia para o objeto a ser testado
    CodigoCarona *codigo;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio cidade
class TUCidade {

private:

    string VALOR_VALIDO = "Anap olis.";
    string VALOR_INVALIDO = ".Brasi  lia";

    // Referencia para o objeto a ser testado
    Cidade *cidade;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio estado
class TUEstado {

private:

    string VALOR_VALIDO = "AM";
    string VALOR_INVALIDO = "P";

    // Referencia para o objeto a ser testado
    Estado *estado;

    // Estado do teste
    int state;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio data
class TUData {

private:

    string VALOR_VALIDO = "28/02/2001";
    string VALOR_INVALIDO = "30/02/2000";

    // Referencia para o objeto a ser testado
    Data *data;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio duracao
class TUDuracao {

private:

    const static int VALOR_VALIDO = 12;
    const static int VALOR_INVALIDO = 56;

    // Referencia para o objeto a ser testado
    Duracao *duracao;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio vagas
class TUVagas {

private:

    const static int VALOR_VALIDO = 3;
    const static int VALOR_INVALIDO = 9;

    // Referencia para o objeto a ser testado
    Vagas *vagas;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio preco
class TUPreco {

private:

    constexpr static float VALOR_VALIDO = 3054.89;
    constexpr static float VALOR_INVALIDO = 6999.9;

    // Referencia para o objeto a ser testado
    Preco *preco;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio codigo de reserva
class TUCodigoReserva {

private:

    string VALOR_VALIDO = "19478";
    string VALOR_INVALIDO = "112500";

    // Referencia para o objeto a ser testado
    CodigoReserva *codigo_reserva;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio assento
class TUAssento {

private:

    static const char VALOR_VALIDO = 'D';
    static const char VALOR_INVALIDO = 'J';

    // Referencia para o objeto a ser testado
    Assento *assento;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio bagagem
class TUBagagem {

private:

    const static int VALOR_VALIDO = 2;
    const static int  VALOR_INVALIDO = 6;

    // Referencia para o objeto a ser testado
    Bagagem *bagagem;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio codigo de banco
class TUBanco {

private:

    string VALOR_VALIDO = "666";
    string  VALOR_INVALIDO = "12";

    // Referencia para o objeto a ser testado
    Banco *banco;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio numero de agencia
class TUAgencia {

private:

    string VALOR_VALIDO = "23721";
    string  VALOR_INVALIDO = "23725";

    // Referencia para o objeto a ser testado
    Agencia *agencia;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio numero da conta
class TUNumeroConta {

private:

    string VALOR_VALIDO = "1234566";
    string  VALOR_INVALIDO = "666";

    // Referencia para o objeto a ser testado
    NumeroConta *numero;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio nome
class TUNome {

private:

    string VALOR_VALIDO = "Clara Resende.";
    string  VALOR_INVALIDO = "C lara  Resen.de";

    // Referencia para o objeto a ser testado
    Nome *nome;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio email
class TUEmail {

private:

    string VALOR_VALIDO = "clara@emaill";
    string  VALOR_INVALIDO = ".clara@ema..il";

    // Referencia para o objeto a ser testado
    Email *email;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio senha
class TUSenha {

private:

    string VALOR_VALIDO = "a1b2c";
    string  VALOR_INVALIDO = "kkkk.";

    // Referencia para o objeto a ser testado
    Senha *senha;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio cpf
class TUCpf {

private:
    string VALOR_VALIDO = "12345678903";
    string  VALOR_INVALIDO = "6661112223";

    // Referencia para o objeto a ser testado
    Cpf *cpf;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Teste para o dominio telefone
class TUTelefone {

private:
    string VALOR_VALIDO = "55-77-988557766";
    string  VALOR_INVALIDO = "00-45-123456789";

    // Referencia para o objeto a ser testado
    Telefone *telefone;

    // Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();
    void testarCenarioFalha();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

#endif

