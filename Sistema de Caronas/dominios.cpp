#include "dominios.h"

// Definição de metodos da classe CodigoCarona

void CodigoCarona::validar(string codigo) {

    // Verifica se o tamanho do codigo esta no formato certo: XXXX
    if(codigo.length() != TAMANHO_CODIGO_CARONA)
        throw invalid_argument("O codigo inserido esta no formato incorreto !");

    // Verifica se o codigo contem apenas numeros
    for (int i=0; i<TAMANHO_CODIGO_CARONA; i++) {
        if(codigo[i] < '0' || codigo[i] > '9')
            throw invalid_argument("O codigo inserido deve conter apenas numeros !");
    }
}

// Método setter da classe codigo
void CodigoCarona::setCodigo(string codigo) {
    validar(codigo);
    this->codigo = codigo;
}

// Definicao de metodos da classe Cidade

void Cidade::validar(string cidade) {

    // Valida se a string esta no tamanho correto
    if(cidade.length() < TAMANHO_MIN_CARACTERES || cidade.length() > TAMANHO_MAX_CARACTERES)
        throw invalid_argument("A cidade inserida esta no formato incorreto: muitos caracteres !");

    // No caso da string ter apenas um caracter, verifica se ele eh uma letra
    for(int i=0; i<TAMANHO_MIN_CARACTERES; i++) {
        if(cidade[i] < 'A' || (cidade[i] > 'Z' && cidade[i] < 'a') || cidade[i] > 'z')
            throw invalid_argument("Cidade no formato incorreto: pelo menos um caracter eh letra !");
    }

    // Verifica se o caracter eh diferente de letra, ponto ou espaco
    for(int i=0; i<TAMANHO_MAX_CARACTERES; i++) {
        if(cidade[i] < 'A' && (cidade[i] > 'Z' && cidade[i] < 'a') && cidade[i] > 'z' && cidade[i] != '.' && cidade[i] != ' ') {
            throw invalid_argument("Caracter invalido !");
        }
        if(cidade[i] == ' ' && cidade[i+1] == ' ')
            throw invalid_argument("Nao pode haver espacos em sequencia !");
    }
}

// Método setter da classe cidade
void Cidade::setCidade(string cidade) {
    validar(cidade);
    this->cidade = cidade;
}

// Definicao de metodos da classe CidadeDestino

void Estado::validar(string estado) {

    // Compara a variável estado a uma das 27 opções disponíveis
   if(!(estado == "AC" || estado == "AL" || estado == "AP" || estado == "AM" ||
         estado == "BA" || estado == "CE" || estado == "DF" || estado == "ES" ||
         estado == "GO" || estado == "MA" || estado == "MT" || estado == "MS" ||
         estado == "MG" || estado == "PA" || estado == "PB" || estado == "PR" ||
         estado == "PE" || estado == "PI" || estado == "RJ" || estado == "RN" ||
         estado == "RS" || estado == "RO" || estado == "RR" || estado == "SC" ||
         estado == "SP" || estado == "SE" || estado == "TO" ))
        throw invalid_argument("Estado inválido.");
}

// Método setter da classe estado
void Estado::setEstado(string estado) {
    validar(estado);
    this->estado = estado;
}

// Definicao de metodos da classe Data

void Data::validar(string data) {

    // Divide a string de data entre dia, mes e ano, sendo a data no formato DD/MM/AAAA
    // Usa a funçao substr para segmentar a string com base na posição do caracter '/'
    string dia, mes, ano;
    int pos = data.find("/");
    // Gera uma string que comeca na posicao 0 de data e vai ate o primeiro caracter '/'
    dia = data.substr(0, pos);

     // Gera uma string que comeca apos o primeiro caracter '/' e vai ate o segundo '/'
    mes = data.substr(pos+1, pos);

    // Gera uma string intermediaria, que comeca no primeiro caracter apos o '/'
    string meio = data.substr(pos+1);

    // Gera uma string que vai do segundo caracter '/' de data, ate o ultimo caracter da string
    ano = meio.substr(pos+1);

    // Verifica se o ano, o mes e o dia da data sao validos, respectivamente
    stringstream geek(ano); // Transforma a string em um int
    int ano_int = 0;
    geek >> ano_int;
    if((ano_int >= 2000 && ano_int <= 2099) && ano_int % 4 != 0){
        if(mes == "01" || mes == "03" || mes == "05" || mes == "07" || mes == "08" || mes == "10" || mes == "12") {
            if (dia > "31" || dia < "01")
                throw invalid_argument("Dia invalido !");
        }
        else if(mes == "04" || mes == "06" || mes == "09" || mes == "11") {
            if (dia > "30" || dia < "01")
                throw invalid_argument("Dia invalido !");
        }
        else if(mes == "02") {
            if (dia > "28" || dia < "01")
                throw invalid_argument("Dia invalido !");
        }
        else{
            throw invalid_argument("O mes é invalido !");
        }
    }

    // Verifica a ocorrencia de ano bissexto
    else if((ano_int >= 2000 && ano_int <= 2099) && ano_int % 4 == 0) {
        if(mes == "01" || mes == "03" || mes == "05" || mes == "07" || mes == "08" || mes == "10" || mes == "12") {
            if (dia > "31" || dia < "01")
                throw invalid_argument("Dia invalido !");
        }
        else if(mes == "04" || mes == "06" || mes == "09" || mes == "11") {
            if (dia > "30" || dia < "01")
                throw invalid_argument("Dia invalido !");
        }
        else if(mes == "02") {
            if (dia > "29" || dia < "01")
                throw invalid_argument("Dia invalido !");
        }
        else{
            throw invalid_argument("O mes é invalido !");
        }
    }
    else {
         throw invalid_argument("O ano é invalido !");
    }
}

// Método setter da classe data
void Data::setData(string data) {
    validar(data);
    this->data = data;
}

// Definicao de metodos da classe Duracao

void Duracao::validar(int duracao) {

    // Determina se o tempo de duracao esta no intervalo correto
    if(duracao < DURACAO_MIN || duracao > DURACAO_MAX)
        throw invalid_argument("O tempo de viagem inserido é inválido !");
}

// Método setter da classe duração da viagem
void Duracao::setDuracao(int duracao) {
    validar(duracao);
    this->duracao = duracao;
}

// Definicao de metodos da classe Vagas

void Vagas::validar(int vagas) {

    // Verifica se nao excedeu o numero de vagas
    if(vagas < VAGAS_MIN || vagas > VAGAS_MAX)
        throw invalid_argument("Numero de vagas invalido !");
}

// Método setter da classe vagas
void Vagas::setVagas(int vagas) {
    validar(vagas);
    this->vagas = vagas;
}

// Definicao de metodos da classe Preco

void Preco::validar(float preco) {

    // Verifica se o preco esta no intervalo correto
    if(preco < PRECO_MIN || preco > PRECO_MAX)
        throw invalid_argument("Preco invalido !");
}

// Método setter da classe preço
void Preco::setPreco(float preco) {
    validar(preco);
    this->preco = preco;
}

// Definição de metodos da classe CodigoReserva

void CodigoReserva::validar(string codigo_reserva) {

    //Verifica se o tamanho do codigo esta no formato certo: XXXXX
    if(codigo_reserva.length() != TAMANHO_CODIGO_RESERVA)
        throw invalid_argument("O codigo inserido esta no formato incorreto !");

    //Verifica se o codigo contem apenas numeros
    for (int i=0; i<TAMANHO_CODIGO_RESERVA; i++) {
        if(codigo_reserva[i] < '0' || codigo_reserva[i] > '9')
            throw invalid_argument("O codigo inserido deve conter apenas numeros !");
    }
}

// Método setter da classe codigo de reserva
void CodigoReserva::setCodigoReserva(string codigo_reserva) {
    validar(codigo_reserva);
    this->codigo_reserva = codigo_reserva;
}

// Definicao de metodos da classe Assento

void Assento::validar(char assento) {

    // Existem apenas dois estados para a variável assento
    if(!(assento == 'D' || assento == 'T'))
        throw invalid_argument("Assento invalido !");
}

// Método setter da classe assento
void Assento::setAssento(char assento) {
    validar(assento);
    this->assento = assento;
}

// Definicao de metodos da classe Bagagem

void Bagagem::validar(int bagagem) {

    // Verifica se o número de bagagem não foi excedido
    if(bagagem < VALOR_MINIMO || bagagem > VALOR_MAXIMO)
        throw invalid_argument("Numero de bagagem invalido !");
}

// Método setter da classe bagagem
void Bagagem::setBagagem(int bagagem) {
    validar(bagagem);
    this->bagagem = bagagem;
}

// Definicao de metodos da classe banco

void Banco::validar(string banco) {

    //Verifica se o tamanho do codigo de banco esta no formato certo: XXX
    if(banco.length() != TAMANHO_CODIGO_BANCO)
        throw invalid_argument("O codigo inserido esta no formato incorreto !");

    //Verifica se o codigo contem apenas numeros
    for (int i=0; i<TAMANHO_CODIGO_BANCO; i++) {
        if(banco[i] < '0' || banco[i] > '9')
            throw invalid_argument("O codigo inserido deve conter apenas numeros !");
    }
}

// Método setter da classe banco
void Banco::setBanco(string banco) {
    validar(banco);
    this->banco = banco;
}

// Definicao de metodos da classe agencia

void Agencia::validar(string agencia) {

    // Verifica se o numero de agencia esta no tamanho correto XXXXX
    if(agencia.length() != TAMANHO_MAX)
        throw invalid_argument("O numero de agencia inserido esta no formato incorreto !");

    //Verifica se o numero de agencia contem apenas numeros
    else {
        int soma = 0;
        int numero = 0;
        for (int i=0; i<TAMANHO_MAX; i++) {
            if(agencia[i] < '0' || agencia[i] > '9')
                throw invalid_argument("O valor inserido deve conter apenas numeros !");

            // Algoritmo de Luhn para verificar digito
            numero = agencia[i] - 48; // Transforma char em int

            if((TAMANHO_MAX - i) % 2 == 0) {
                numero *= 2;
                if(numero > 9)
                    numero -= 9;
            }
            soma += numero;
        }
        if(soma % 10 != 0)
            throw invalid_argument("Número inválido !");
    }
}

// Método setter da classe agencia
void Agencia::setAgencia(string agencia) {
    validar(agencia);
    this->agencia = agencia;
}

// Definicao de metodos da classe conta

void NumeroConta::validar(string numero) {

     // Verifica se o numero da conta esta no tamanho correto XXXXXXX
    if(numero.length() != TAMANHO_MAX)
        throw invalid_argument("O numero da conta inserido esta no formato incorreto !");

    //Verifica se o numero da conta contem apenas numeros
    else {
        int soma = 0;
        int digito = 0;
        for (int i=0; i<TAMANHO_MAX; i++) {
            if(numero[i] < '0' || numero[i] > '9')
                throw invalid_argument("O valor inserido deve conter apenas numeros !");

            // Algoritmo de Luhn para verificar digito
            digito = numero[i] - 48; // Transforma char em int

            if((TAMANHO_MAX - i) % 2 == 0) {
                digito *= 2;
                if(digito > 9)
                    digito -= 9;
            }
            soma += digito;
        }
        if(soma % 10 != 0)
            throw invalid_argument("Número inválido !");
    }
}

// Método setter da classe conta
void NumeroConta::setNumeroConta(string numero) {
    validar(numero);
    this->numero = numero;
}

// Definicao de metodos da classe nome

void Nome::validar(string nome) {

     // valida se a string esta no tamanho correto
    if(nome.length() < TAMANHO_MIN_CARACTERES || nome.length() > TAMANHO_MAX_CARACTERES)
        throw invalid_argument("O nome inserido esta no formato incorreto: muitos caracteres !");

    // no caso da string ter apenas um caracter, verifica se ele eh uma letra
    for(int i=0; i<TAMANHO_MIN_CARACTERES; i++) {
        if(nome[i] < 'A' || (nome[i] > 'Z' && nome[i] < 'a') || nome[i] > 'z')
            throw invalid_argument("Nome no formato incorreto: pelo menos um caracter eh letra !");
    }

    // verifica se o caracter eh diferente de letra, ponto ou espaco
    for(int i=0; i<TAMANHO_MAX_CARACTERES; i++) {
        if(nome[i] < 'A' && (nome[i] > 'Z' && nome[i] < 'a') && nome[i] > 'z' && nome[i] != '.' && nome[i] != ' ') {
            throw invalid_argument("Caracter invalido !");
        }
        if(nome[i] == ' ' && nome[i+1] == ' ')
            throw invalid_argument("Nao pode haver espacos em sequencia !");
    }
}

// Método setter da classe nome
void Nome::setNome(string nome) {
    validar(nome);
    this->nome = nome;
}

// Definicao de metodos da classe email

void Email::validar(string email) {

    // O sinal pos indica a posição do caracter '@' na string email
    // A string é subdividida em uma parte local e um dominio
    int pos = email.find('@');
    int fin = email.length();
    string local = email.substr(0, pos);
    string dominio = email.substr(pos+1, fin-1);

    int fin1 = local.length();

    // Verifica se o tamanho da substring local é válido
    if(fin1 > 20)
            throw invalid_argument("Tamanho inválido !");

    for(int i=0; i<fin1; i++){

        // Verifca se a string é composta apenas por letras e pontos
        if((local[i] < 'a' || local[i] > 'z') && local[i] != '.')
            throw invalid_argument("Caracter inválido!");

        if(local[0] == '.' || local[fin1-1] == '.')
            throw invalid_argument("Nao pode comecar ou terminar com ponto !");

         if(local[i] == '.' && local[i+1] == '.')
            throw invalid_argument("Nao pode haver pontos em sequencia !");
    }

    int fin2 = dominio.length();

    // Verifica se o tamanho da string dominio é válido
    if(fin2 > 20)
            throw invalid_argument("Tamanho inválido !");

    for(int j=0; j<fin2; j++){

        // Verifca se a string é composta apenas por letras e pontos
        if((dominio[j] < 'a' || dominio[j] > 'z') && dominio[j] != '.')
            throw invalid_argument("Caracter inválido!");

        if(dominio[0] == '.')
            throw invalid_argument("Nao pode comecar com ponto !");

        if(dominio[j] == '.' && dominio[j+1] == '.')
            throw invalid_argument("Nao pode haver pontos em sequencia !");
    }
}

// Método setter da classe email
void Email::setEmail(string email) {
    validar(email);
    this->email = email;
}

// Definicao de metodos da classe senha

void Senha::validar(string senha){
    int i, sinal = 0;
    bool minusculo = 0;
    bool maiusculo = 0;
    bool numero = 0;
    bool caracter = 0;
    int lim = senha.length();

    //
    // Verifica se o tamanho está incorreto e logo em seguida se contem algum caracter invalido
    // se achado algum caracter invalido a variavel booleana caracter_valido permanece falsa ativando um throw
    // no final da execução da validação.
    //
    if(senha.length() != TAMANHO)
        throw invalid_argument("Senha inválida: tamanho incorreto");
    else {
        for(i = 0; i < lim; i++){
            if((senha[i] >= 'a' && senha[i] <= 'z') || (senha[i] >= 'A' && senha[i] <= 'Z') || (senha[i] >= '0' && senha[i] <= '9') || (senha[i] == '#') || (senha[i] == '$') || (senha[i] == '%') || (senha[i] == '&')){
                if(senha[i] >= 'a' && senha[i] <= 'z')
                    minusculo = 1;
                if(senha[i] >= 'A' && senha[i] <= 'Z')
                    maiusculo = 1;
                if(senha[i] >= '0' && senha[i] <= '9')
                    numero = 1;
                if(senha[i] == '#' || senha[i] == '$' || senha[i] == '%' || senha[i] == '&')
                    caracter = 1;
            }
            else {
                throw invalid_argument("Caracter inválido !");
            }
        }

        if(!minusculo && !maiusculo)
            throw invalid_argument("Senha inválida: nenhuma letra.");

        if(!numero)
            throw invalid_argument("Senha inválida: nenhum caracter numérico.");

        for(i=0; i<lim; i++){
            for(int j=0; j<lim; j++){
                if(senha[i] == senha[j])
                    sinal++;
            }
            if(sinal > 1)
                throw invalid_argument("Caracter repetido !");
            sinal = 0;
        }
    }
}

// Método setter da classe senha
void Senha::setSenha(string senha) {
    validar(senha);
    this->senha = senha;
}

// Definicao de metodos da classe Cpf

void Cpf::validar(string cpf) {
    // Verificando se o tamanho do CPF está correto.
    if(cpf.length() != TAMANHO)
        throw invalid_argument("CPF inválido: tamanho errado.");
    else{
        int soma = 0;
        int digito;

        // Verificando se dígitos são estritamente numéricos e se
        // o CPF passa no algoritmo de Luhn.
        for(int i = 0; i < TAMANHO; i++){
            if(cpf[i] < '0' || cpf[i] > '9')
                throw invalid_argument("CPF inválido: caracteres não numéricos.");

            digito = cpf[i] - '0';

            //Fazendo a soma dos dígitos de acordo com o algoritmo de Luhn.
            if((TAMANHO-i)%2 == 0){
                digito *= 2;
                if(digito > 9)
                    digito -= 9;
            }
            soma += digito;
        }
        if(soma % 10 != 0)
            throw invalid_argument("CPF inválido: não passa no algoritmo de Luhn.\n");
        }
}

// Método setter da classe cpf
void Cpf::setCpf(string cpf) {
    validar(cpf);
    this->cpf = cpf;
}

// Definicao de metodos da classe telefone

void Telefone::validar(string telefone) {

    // A string de telefone é dividida em substrings,
    // uma para o DDI, DDD e o numero de 9 digitos.
    // É utilizada a função substr da biblioteca "string"
    // para separar a string telefone em componentes menores.
    // Os sinais pos1 e pos2 indicam onde estão localizados os caracteres '-' que dividem a string telefone
    int pos1 = telefone.find('-');
    int tamanho = telefone.length();
    string DDI = telefone.substr(0, pos1);
    string meio = telefone.substr(pos1+1, tamanho-1);
    int pos2 = meio.find('-');
    string DDD = meio.substr(0, pos2);
    string numero = meio.substr(pos2+1, tamanho-1);

    // São testados as condições para que o telefone seja válido ou não
    if (DDI == "00")
        throw invalid_argument("Numero inválido !");
    if (DDD == "00")
        throw invalid_argument("Numero inválido !");
    if (numero == "000000000")
        throw invalid_argument("Numero inválido !");

    // Verifica se a string possui 13 dígitos e dois caracteres '-'
    if (telefone.length() != 15)
        throw invalid_argument("Tamanho de telefone inválido !");
}

// Método setter da classe telefone
void Telefone::setTelefone(string telefone) {
    validar(telefone);
    this->telefone = telefone;
}
