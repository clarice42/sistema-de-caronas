#include <iostream>
#include "entidades.h"
#include "dominios.h"
#include "testesDominio.h"
#include "testesEntidade.h"
using namespace std;

int main()
{

    // Testes de domínio

    TUCodigoCarona teste1;

    switch(teste1.run()){
        case TUCodigoCarona::SUCESSO: cout << "Teste 1: SUCESSO";
                                break;
        case TUCodigoCarona::FALHA  : cout << "Teste 1: FALHA";
                                break;
    }

    TUCidade teste2;

    switch(teste2.run()){
        case TUCidade::SUCESSO: cout << "\nTeste 2: SUCESSO";
                            break;
        case TUCidade::FALHA  : cout << "\nTeste 2: FALHA";
                            break;
    }

    TUEstado teste3;

    switch(teste3.run()){
        case TUEstado::SUCESSO: cout << "\nTeste 3: SUCESSO";
                            break;
        case TUEstado::FALHA  : cout << "\nTeste 3: FALHA";
                            break;
    }

    TUData teste4;

    switch(teste4.run()){
        case TUData::SUCESSO: cout << "\nTeste 4: SUCESSO";
                           break;
        case TUData::FALHA  : cout << "\nTeste 4: FALHA";
                           break;
    }

    TUDuracao teste5;

    switch(teste5.run()){
        case TUDuracao::SUCESSO: cout << "\nTeste 5: SUCESSO";
                           break;
        case TUDuracao::FALHA  : cout << "\nTeste 5: FALHA";
                           break;
    }

    TUVagas teste6;

    switch(teste6.run()){
        case TUVagas::SUCESSO: cout << "\nTeste 6: SUCESSO";
                           break;
        case TUVagas::FALHA  : cout << "\nTeste 6: FALHA";
                           break;
    }

    TUPreco teste7;

    switch(teste7.run()){
        case TUPreco::SUCESSO: cout << "\nTeste 7: SUCESSO";
                           break;
        case TUPreco::FALHA  : cout << "\nTeste 7: FALHA";
                           break;
    }

    TUCodigoReserva teste8;

    switch(teste8.run()){
        case TUCodigoReserva::SUCESSO: cout << "\nTeste 8: SUCESSO";
                           break;
        case TUCodigoReserva::FALHA  : cout << "\nTeste 8: FALHA";
                           break;
    }

    TUAssento teste9;

    switch(teste9.run()){
        case TUAssento::SUCESSO: cout << "\nTeste 9: SUCESSO";
                           break;
        case TUAssento::FALHA  : cout << "\nTeste 9: FALHA";
                           break;
    }

    TUBagagem teste10;

    switch(teste10.run()){
        case TUBagagem::SUCESSO: cout << "\nTeste 10: SUCESSO";
                           break;
        case TUBagagem::FALHA  : cout << "\nTeste 10: FALHA";
                           break;
    }

    TUBanco teste11;

    switch(teste11.run()){
        case TUBanco::SUCESSO: cout << "\nTeste 11: SUCESSO";
                           break;
        case TUBanco::FALHA  : cout << "\nTeste 11: FALHA";
                           break;
    }

    TUAgencia teste12;

    switch(teste12.run()){
        case TUAgencia::SUCESSO: cout << "\nTeste 12: SUCESSO";
                           break;
        case TUAgencia::FALHA  : cout << "\nTeste 12: FALHA";
                           break;
    }

    TUNumeroConta teste13;

    switch(teste13.run()){
        case TUNumeroConta::SUCESSO: cout << "\nTeste 13: SUCESSO";
                           break;
        case TUNumeroConta::FALHA  : cout << "\nTeste 13: FALHA";
                           break;
    }

    TUNome teste14;

    switch(teste14.run()){
        case TUNome::SUCESSO: cout << "\nTeste 14: SUCESSO";
                           break;
        case TUNome::FALHA  : cout << "\nTeste 14: FALHA";
                            break;
    }

    TUEmail teste15;

    switch(teste15.run()){
        case TUEmail::SUCESSO: cout << "\nTeste 15: SUCESSO";
                           break;
        case TUEmail::FALHA  : cout << "\nTeste 15: FALHA";
                           break;
    }

    TUSenha teste16;

    switch(teste16.run()){
        case TUSenha::SUCESSO: cout << "\nTeste 16: SUCESSO";
                           break;
        case TUSenha::FALHA  : cout << "\nTeste 16: FALHA";
                           break;
    }

    TUCpf teste17;

    switch(teste17.run()){
        case TUCpf::SUCESSO: cout << "\nTeste 17: SUCESSO";
                           break;
        case TUCpf::FALHA  : cout << "\nTeste 17: FALHA";
                           break;
    }

    TUTelefone teste18;

    switch(teste18.run()){
        case TUTelefone::SUCESSO: cout << "\nTeste 18: SUCESSO";
                           break;
        case TUTelefone::FALHA  : cout << "\nTeste 18: FALHA";
                           break;
    }

    // Testes de entidade

    TUUsuario teste19;

    switch(teste19.run()){
        case TUUsuario::SUCESSO: cout << "\nTeste 19: SUCESSO";
                           break;
        case TUUsuario::FALHA  : cout << "\nTeste 19: FALHA";
                           break;
    }

    TUReserva teste20;

    switch(teste20.run()){
        case TUReserva::SUCESSO: cout << "\nTeste 20: SUCESSO";
                           break;
        case TUReserva::FALHA  : cout << "\nTeste 20: FALHA";
                           break;
    }

    TUCarona teste21;

    switch(teste21.run()){
        case TUCarona::SUCESSO: cout << "\nTeste 21: SUCESSO";
                           break;
        case TUCarona::FALHA  : cout << "\nTeste 21: FALHA";
                           break;
    }

    TUConta teste22;

    switch(teste22.run()){
        case TUConta::SUCESSO: cout << "\nTeste 22: SUCESSO";
                           break;
        case TUConta::FALHA  : cout << "\nTeste 22: FALHA";
                           break;
    }


    return 0;
}
