#ifndef ENTIDADES_H_INCLUDED
#define ENTIDADES_H_INCLUDED
#include <string>
#include "dominios.h"

using namespace std;

/// \brief Declaracao de classe da entidade Carona
///
/// \param codigo - codigo da carona
/// \param cidade_origem - cidade de origem da carona
/// \param cidade_destino - cidade de destino da carona
/// \param estado_origem - estado de origem da carona
/// \param estado_destino - estado de destino da carona
/// \param data - data que a carona sera realizada
/// \param duracao - duracao do tempo de viagem da carona
/// \param vagas - numero de vagas na carona
/// \param preco - preco pela carona
/// \return Nao realiza nenhum retorno
///
///
class Carona {

private:
    ///Declara o codigo como membro da classe de dominio CodigoCarona
    CodigoCarona codigo;
    ///Declara a cidade_origem como membro da classe de dominio Cidade
    Cidade cidade_origem;
    ///Declara a cidade_destino como membro da classe de dominio Cidade
    Cidade cidade_destino;
    ///Declara o estado_origem como membro da classe de dominio Estado
    Estado estado_origem;
    ///Declara o estado_destino como membro da classe de dominio Estado
    Estado estado_destino;
    ///Declara a data como membro da classe de dominio Data
    Data data;
    ///Declara a duracao como membro da classe de dominio Duracao
    Duracao duracao;
    ///Declara as vagas como membro da classe de dominio Vagas
    Vagas vagas;
    ///Declara o preco como membro da classe de dominio Preco
    Preco preco;

public:
    // Metodo setter e getter para  codigo de carona
    ///Metodo Setter para o Codigo da carona
    /// \param codigo da carona
    void setCodigo(const CodigoCarona &codigo) {
        this->codigo = codigo;
    }
    ///Metodo Getter para o Codigo da Carona
    /// \return codigo da carona
    CodigoCarona getCodigo() const {
        return codigo;
    }
    // Metodo setter e getter para a cidade de origem
    ///Metodo Setter para a Cidade de origem
    /// \param cidade de origem
    void setCidadeOrigem(const Cidade &cidade_origem) {
        this->cidade_origem = cidade_origem;
    }
    ///Metodo Getter para a Cidade de origem
    /// \return cidade de origem
    Cidade getCidadeOrigem() const {
        return cidade_origem;
    }
    // Metodo setter e getter para a cidade de destino
    ///Metodo Setter para a Cidade de destino
    /// \param cidade de destino
    void setCidadeDestino(const Cidade &cidade_destino) {
        this->cidade_destino = cidade_destino;
    }
    ///Metodo Getter para a Cidade de destino
    /// \return cidade de destino
    Cidade getCidadeDestino() const {
        return cidade_destino;
    }
    // Metodo setter e getter para o estado de origem
    ///Metodo Setter para o Estado de origem
    /// \param estado de origem
    void setEstadoOrigem(const Estado &estado_origem) {
        this->estado_origem = estado_origem;
    }
    ///Metodo Getter para o Estado de origem
    /// \return estado de origem
    Estado getEstadoOrigem() const {
        return estado_origem;
    }
    // Metodo setter e getter para o estado de destino
    ///Metodo Setter para o Estado de origem
    /// \param estado de destino
    void setEstadoDestino(const Estado &estado_destino) {
        this->estado_destino = estado_destino;
    }
    ///Metodo Getter para o Estado de destino
    /// \return estado de destino
    Estado getEstadoDestino() const {
        return estado_destino;
    }
    // Metodo setter e getter para a data
    ///Metodo Setter para a Data
    /// \param data da carona
    void setData(const Data &data) {
        this->data = data;
    }
    ///Metodo Getter para a Data
    /// \return data da carona
    Data getData() const {
        return data;
    }
    // Metodo setter e getter para a duracao da carona
    ///Metodo Setter para a Duracao
    /// \param duracao da carona
    void setDuracao(const Duracao &duracao) {
        this->duracao = duracao;
    }
    ///Metodo Getter para a Duracao
    /// \return duracao da carona
    Duracao getDuracao() const {
        return duracao;
    }
    // Metodo setter e getter para o numero de vagas
    ///Metodo Setter para as Vagas
    /// \param vagas
    void setVagas(const Vagas &vagas) {
        this->vagas = vagas;
    }
    ///Metodo Getter para as Vagas
    /// \return vagas
    Vagas getVagas() const {
        return vagas;
    }
    // Metodo setter e getter para o preco
    ///Metodo Setter para o preco
    /// \param preco da carona
    void setPreco(const Preco &preco) {
        this->preco = preco;
    }
    ///Metodo Getter para o preco
    /// \return preco da carona
    Preco getPreco() const {
        return preco;
    }
};


/// \brief Declaracao de classe da entidade Reserva
///
/// \param codigo - codigo da reserva feita
/// \param assento - assento da reserva
/// \param bagagem - numero que bagagens na reserva
/// \return Nao faz nenhuma retorno
///
///

class Reserva {


private:
    ///Declara codigo como membro de classe do dominio CodigoReserva
    CodigoReserva codigo;
    ///Declara assento como membro de classe do dominio Assento
    Assento assento;
    ///Declara bagagem como membro de classe do dominio Bagagem
    Bagagem bagagem;

public:
    // Metodo setter e getter para  codigo de reserva
    /// \param codigo de reserva
    ///Metodo Setter para o Codigo de Reserva
    void setCodigoReserva(const CodigoReserva &codigo) {
        this->codigo = codigo;
    }
    ///Metodo Getter para o Codigo de Reserva
    /// \return codigo de reserva
    CodigoReserva getCodigoReserva() const {
        return codigo;
    }

    // Metodo setter e getter para assento
    ///Metodo Setter para o numero do Assento
    /// \param numero do assento
    void setAssento(const Assento &assento) {
        this->assento = assento;
    }
    ///Metodo Getter para o numero de Assento
    /// \return numero do assento
    Assento getAssento() const {
        return assento;
    }

    // Metodo setter e getter para bagagem
    ///Metodo Setter para o numero de bagagens
    /// \param numero de bagagens
    void setBagagem(const Bagagem &bagagem) {
        this->bagagem = bagagem;
    }
    ///Metodo Getter para o numero de bagagens
    /// \return numero de bagagens
    Bagagem getBagagem() const {
        return bagagem;
    }

};

/// \brief Declaracao de classe da entidade Conta
///
/// \param banco - codigo do banco referente a conta
/// \param agencia - codigo da agencia referente a conta
/// \param numero - numero da conta
/// \return Nao faz qualquer retorno
///
///
class Conta {

private:
    ///Declara banco como membro da classe de dominio Banco
    Banco banco;
    ///Declara agencia como membro da classe de dominio Agencia
    Agencia agencia;
    ///Declara numero como membro da classe de dominio NumeroConta
    NumeroConta numero;

public:
     // Metodo setter e getter para numero do banco
    ///Metodo Setter para o codigo do banco
    /// \param codigo do banco
    void setBanco(const Banco &banco) {
        this->banco = banco;
    }
    ///Metodo Getter para o codigo do banco
    /// \return codigo do banco
    Banco getBanco() const {
        return banco;
    }

    // Metodo setter e getter para numero da agencia
    ///Metodo Setter para o numero da agencia
    /// \param numero da agencia
    void setAgencia(const Agencia &agencia) {
        this->agencia = agencia;
    }
    ///Metodo Getter para o numero da agencia
    /// \return nemero da agencia
    Agencia getAgencia() const {
        return agencia;
    }

    // Metodo setter e getter para o numero da conta
    ///Metodo Setter para o numero da conta
    /// \param numero da conta
    void setNumeroConta(const NumeroConta &numero) {
        this->numero = numero;
    }
    ///Metodo Getter para o numero da conta
    /// \return numero da conta
    NumeroConta getNumeroConta() const {
        return numero;
    }
};



/// \brief Declaracao de classe da entidade Usuario
///
/// \param nome - nome do usuario
/// \param email - email do usuario
/// \param senha - senha do usuario
/// \param telefone - telfone do usuario
/// \param cpf - cpf do usuario
/// \return nao faz nenhuma retorno
///
///

class Usuario {

private:
    ///Declara nome como membro da classe de dominio Nome
    Nome nome;
    ///Declara email como membro da classe de dominio Email
    Email email;
    ///Declara senha como membro da classe de dominio Senha
    Senha senha;
    ///Declara telefone como membro da classe de dominio Telefone
    Telefone telefone;
    ///Declara cpf como membro da classe de dominio Cpf
    Cpf cpf;

public:

    //setters
    ///Metodo Setter para o nome do usuario
    /// \param nome do usuario
    void setNome(Nome &nome) {
        this->nome = nome;
    }
    ///Metodo Getter para o email do usuario
    /// \param email do usuario
    void setEmail(Email &email) {
        this->email = email;
    }
    ///Metodo Setter para a senha do usuario
    /// \param senha do usuario
    void setSenha(Senha &senha) {
        this->senha = senha;
    }
    ///Metodo Setter para o cpf do usuario
    /// \param cpf do usuario
    void setCpf(Cpf &cpf) {
        this->cpf = cpf;
    }
    ///Metodo Setter para o telefone do usuario
    /// \param telefone do usuario
    void setTelefone(Telefone &telefone){
        this->telefone = telefone;
    }

    //getters
    ///Metodo Getter para o nome do usuario
    /// \return nome do usuario
    Nome getNome() const {
        return nome;
    }
    ///Metodo Getter para o email do usuario
    /// \return email do usuario
    Email getEmail() const {
        return email;
    }
    ///Metodo Getter para a senha do usuario
    /// \return senha do usuario
    Senha getSenha() const {
        return senha;
    }
    ///Metodo Getter para o cpf do usuario
    /// \return cpf do usuario
    Cpf getCpf() const {
        return cpf;
    }
    ///Metodo Getter para o telefone do usuario
    /// \return telefone do usuario
    Telefone getTelefone() const {
        return telefone;
    }
};


#endif // ENTIDADES_H_INCLUDED
