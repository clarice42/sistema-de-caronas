#ifndef TESTESENTIDADE_H_INCLUDED
#define TESTESENTIDADE_H_INCLUDED

#include "entidades.h"

// Definição da classe de teste da entidade usuário
class TUUsuario {

private:
	
	// Valores validos para teste
    const string VALOR_VALIDO_NOME = "Clara Resende.";
    const string VALOR_VALIDO_EMAIL = "clara@eu";
    const string VALOR_VALIDO_TELEFONE = "55-62-966666666";
    const string VALOR_VALIDO_SENHA = "a1b2c";
    const string VALOR_VALIDO_CPF = "11133355575";

	// Referencia para os objetos a serem testados
    Usuario *usuario;
    Nome *nome_user;
    Email *email_user;
    Telefone *telefone_user;
    Senha *senha_user;
    Cpf *cpf_user;

	// Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Definição da classe de teste da entidade reserva
class TUReserva {

private:

	// Valores validos para teste
    const string VALOR_VALIDO_RESERVA = "12345";
    const char VALOR_VALIDO_ASSENTO = 'D';
    const static int VALOR_VALIDO_BAGAGEM = 3;

	// Referencia para os objetos a serem testados
    Reserva *reserva;
    CodigoReserva *codigo_reserva;
    Assento *assento_reserva;
    Bagagem *bagagem_reserva;

	// Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Definição da classe de teste da entidade carona
class TUCarona {

private:

	// Valores validos para teste
    const string VALOR_VALIDO_CARONA = "1234";
    const string VALOR_VALIDO_CIDADEO = "Sao Paulo";
    const string VALOR_VALIDO_CIDADED = "Rio";
    const string VALOR_VALIDO_ESTADOO = "SP";
    const string VALOR_VALIDO_ESTADOD = "RJ";
    const string VALOR_VALIDO_DATA = "01/10/2019";
    const static int VALOR_VALIDO_DURACAO = 12;
    const static int VALOR_VALIDO_VAGAS = 2;
    constexpr static float VALOR_VALIDO_PRECO = 560.9;

	// Referencia para os objetos a serem testados
    Carona *carona;
    CodigoCarona *codigo_carona;
    Cidade *cidadeo_carona;
    Cidade *cidaded_carona;
    Estado *estadoo_carona;
    Estado *estadod_carona;
    Data *data_carona;
    Duracao *duracao_carona;
    Vagas *vagas_carona;
    Preco *preco_carona;

	// Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

// Definição da classe de teste da entidade conta
class TUConta {

private:

	// Valores validos para teste
    const string VALOR_VALIDO_BANCO = "123";
    const string VALOR_VALIDO_AGENCIA = "12344";
    const string VALOR_VALIDO_CONTA = "1234566";

	// Referencia para os objetos a serem testados
    Conta *conta;
    Banco *banco_conta;
    Agencia *agencia_conta;
    NumeroConta *numero_conta;

	// Estado do teste
    int estado;

    void setUp();
    void tearDown();
    void testarCenarioSucesso();

public:

    const static int SUCESSO = 0;
    const static int FALHA = -1;

    int run();

};

#endif
