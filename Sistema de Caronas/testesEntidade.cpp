#include "testesEntidade.h"

// Definição de métodos da classe de teste da entidade usuario
void TUUsuario::setUp() {

    // Cria novas instãncias de classes e atribui valores válidos
    usuario = new Usuario;

    nome_user = new Nome;
    nome_user->setNome(VALOR_VALIDO_NOME);

    email_user = new Email;
    email_user->setEmail(VALOR_VALIDO_EMAIL);

    telefone_user = new Telefone;
    telefone_user->setTelefone(VALOR_VALIDO_TELEFONE);

    senha_user = new Senha;
    senha_user->setSenha(VALOR_VALIDO_SENHA);

    cpf_user = new Cpf;
    cpf_user->setCpf(VALOR_VALIDO_CPF);

    estado = SUCESSO;
}

void TUUsuario::tearDown() {

    // Deleta os valores atribuídos as classes
    delete nome_user;
    delete email_user;
    delete telefone_user;
    delete senha_user;
    delete cpf_user;
    delete usuario;
}

void TUUsuario::testarCenarioSucesso() {

    // Verifica se o valor das classes é válido ou não
    usuario->setNome(*nome_user);
    if(usuario->getNome().getNome() != VALOR_VALIDO_NOME)
        estado = FALHA;

    usuario->setEmail(*email_user);
    if(usuario->getEmail().getEmail() != VALOR_VALIDO_EMAIL)
        estado = FALHA;

    usuario->setTelefone(*telefone_user);
    if(usuario->getTelefone().getTelefone() != VALOR_VALIDO_TELEFONE)
        estado = FALHA;

    usuario->setSenha(*senha_user);
    if(usuario->getSenha().getSenha() != VALOR_VALIDO_SENHA)
        estado = FALHA;

    usuario->setCpf(*cpf_user);
    if(usuario->getCpf().getCpf() != VALOR_VALIDO_CPF)
        estado = FALHA;
}

int TUUsuario::run() {
    setUp();
    testarCenarioSucesso();
    tearDown();
    return estado;
}

// Definição de métodos da classe de teste de entidade reserva
void TUReserva::setUp() {

    // Cria novas instãncias de classes e atribui valores válidos
    reserva = new Reserva;

    codigo_reserva = new CodigoReserva;
    codigo_reserva->setCodigoReserva(VALOR_VALIDO_RESERVA);

    assento_reserva = new Assento;
    assento_reserva->setAssento(VALOR_VALIDO_ASSENTO);

    bagagem_reserva = new Bagagem;
    bagagem_reserva->setBagagem(VALOR_VALIDO_BAGAGEM);

    estado = SUCESSO;
}

void TUReserva::tearDown() {

    // Deleta os valores atribuídos as classes
    delete codigo_reserva;
    delete assento_reserva;
    delete bagagem_reserva;
    delete reserva;
}

void TUReserva::testarCenarioSucesso() {

    // Verifica se o valor das classes é válido ou não
    reserva->setCodigoReserva(*codigo_reserva);
    if(reserva->getCodigoReserva().getCodigoReserva() != VALOR_VALIDO_RESERVA)
        estado = FALHA;

    reserva->setAssento(*assento_reserva);
    if(reserva->getAssento().getAssento() != VALOR_VALIDO_ASSENTO)
        estado = FALHA;

    reserva->setBagagem(*bagagem_reserva);
    if(reserva->getBagagem().getBagagem() != VALOR_VALIDO_BAGAGEM)
        estado = FALHA;
}

int TUReserva::run() {
    setUp();
    testarCenarioSucesso();
    tearDown();
    return estado;
}

// Definição de métodos da classe de teste da entidade carona
void TUCarona::setUp() {

    // Cria novas instãncias de classes e atribui valores válidos
    carona = new Carona;

    codigo_carona = new CodigoCarona;
    codigo_carona->setCodigo(VALOR_VALIDO_CARONA);

    cidadeo_carona = new Cidade;
    cidadeo_carona->setCidade(VALOR_VALIDO_CIDADEO);

    cidaded_carona = new Cidade;
    cidaded_carona->setCidade(VALOR_VALIDO_CIDADED);

    estadoo_carona = new Estado;
    estadoo_carona->setEstado(VALOR_VALIDO_ESTADOO);

    estadod_carona = new Estado;
    estadod_carona->setEstado(VALOR_VALIDO_ESTADOD);

    data_carona = new Data;
    data_carona->setData(VALOR_VALIDO_DATA);

    duracao_carona = new Duracao;
    duracao_carona->setDuracao(VALOR_VALIDO_DURACAO);

    vagas_carona = new Vagas;
    vagas_carona->setVagas(VALOR_VALIDO_VAGAS);

    preco_carona = new Preco;
    preco_carona->setPreco(VALOR_VALIDO_PRECO);

    estado = SUCESSO;
}

void TUCarona::tearDown() {

    // Deleta os valores atribuídos as classes
    delete codigo_carona;
    delete cidadeo_carona;
    delete cidaded_carona;
    delete estadoo_carona;
    delete estadod_carona;
    delete data_carona;
    delete duracao_carona;
    delete vagas_carona;
    delete preco_carona;
    delete carona;
}

void TUCarona::testarCenarioSucesso() {

    // Verifica se o valor das classes é válido ou não
    carona->setCodigo(*codigo_carona);
    if(carona->getCodigo().getCodigo() != VALOR_VALIDO_CARONA)
        estado = FALHA;

    carona->setCidadeOrigem(*cidadeo_carona);
    if(carona->getCidadeOrigem().getCidade() != VALOR_VALIDO_CIDADEO)
        estado = FALHA;

    carona->setCidadeDestino(*cidaded_carona);
    if(carona->getCidadeDestino().getCidade() != VALOR_VALIDO_CIDADED)
        estado = FALHA;

    carona->setEstadoOrigem(*estadoo_carona);
    if(carona->getEstadoOrigem().getEstado() != VALOR_VALIDO_ESTADOO)
        estado = FALHA;

    carona->setEstadoDestino(*estadod_carona);
    if(carona->getEstadoDestino().getEstado() != VALOR_VALIDO_ESTADOD)
        estado = FALHA;

    carona->setData(*data_carona);
    if(carona->getData().getData() != VALOR_VALIDO_DATA)
        estado = FALHA;

    carona->setDuracao(*duracao_carona);
    if(carona->getDuracao().getDuracao() != VALOR_VALIDO_DURACAO)
        estado =  FALHA;

    carona->setVagas(*vagas_carona);
    if(carona->getVagas().getVagas() != VALOR_VALIDO_VAGAS)
        estado = FALHA;

    carona->setPreco(*preco_carona);
    if(carona->getPreco().getPreco() != VALOR_VALIDO_PRECO)
        estado = FALHA;
}

int TUCarona::run() {
    setUp();
    testarCenarioSucesso();
    tearDown();
    return estado;
}

// Definição de métodos da classe de teste de entidade conta
void TUConta::setUp() {

    // Cria novas instãncias de classes e atribui valores válidos
    conta = new Conta;

    banco_conta = new Banco;
    banco_conta->setBanco(VALOR_VALIDO_BANCO);

    agencia_conta = new Agencia;
    agencia_conta->setAgencia(VALOR_VALIDO_AGENCIA);

    numero_conta = new NumeroConta;
    numero_conta->setNumeroConta(VALOR_VALIDO_CONTA);

    estado = SUCESSO;
}

void TUConta::tearDown() {

    // Deleta os valores atribuídos as classes
    delete banco_conta;
    delete agencia_conta;
    delete numero_conta;
    delete conta;
}

void TUConta::testarCenarioSucesso() {

    // Verifica se o valor das classes é válido ou não
    conta->setBanco(*banco_conta);
    if(conta->getBanco().getBanco() != VALOR_VALIDO_BANCO)
        estado = FALHA;

    conta->setAgencia(*agencia_conta);
    if(conta->getAgencia().getAgencia() != VALOR_VALIDO_AGENCIA)
        estado = FALHA;

    conta->setNumeroConta(*numero_conta);
    if(conta->getNumeroConta().getNumeroConta() != VALOR_VALIDO_CONTA)
        estado = FALHA;
}

int TUConta::run() {
    setUp();
    testarCenarioSucesso();
    tearDown();
    return estado;
}
