#include "testesDominio.h"

// Metodos do teste de codigo de carona
void TUCodigoCarona::setUp(){
    codigo = new CodigoCarona();
    estado = SUCESSO;
}

void TUCodigoCarona::tearDown(){
    delete codigo;
}

void TUCodigoCarona::testarCenarioSucesso(){
    try{
        codigo->setCodigo(VALOR_VALIDO);
        if (codigo->getCodigo() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUCodigoCarona::testarCenarioFalha(){
    try{
        codigo->setCodigo(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUCodigoCarona::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de cidade
void TUCidade::setUp() {
    cidade = new Cidade();
    estado = SUCESSO;
}

void TUCidade::tearDown() {
    delete cidade;
}

void TUCidade::testarCenarioSucesso() {
    try{
        cidade->setCidade(VALOR_VALIDO);
        if(cidade->getCidade() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao) {
        estado = FALHA;
    }
}

void TUCidade::testarCenarioFalha() {
    try {
        cidade->setCidade(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao) {
        return;
    }
}

int TUCidade::run() {
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de Estado
void TUEstado::setUp() {
    estado = new Estado();
    state = SUCESSO;
}

void TUEstado::tearDown() {
    delete estado;
}

void TUEstado::testarCenarioSucesso() {
    try {
        estado->setEstado(VALOR_VALIDO);
        if(estado->getEstado() != VALOR_VALIDO)
            state = FALHA;
    }
    catch(invalid_argument excecao) {
        state = FALHA;
    }
}

void TUEstado::testarCenarioFalha() {
    try {
        estado->setEstado(VALOR_INVALIDO);
        state = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUEstado::run() {
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return state;
}

// Metodos do teste de data
void TUData::setUp(){
    data = new Data();
    estado = SUCESSO;
}

void TUData::tearDown(){
    delete data;
}

void TUData::testarCenarioSucesso(){
    try{
        data->setData(VALOR_VALIDO);
        if (data->getData() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUData::testarCenarioFalha(){
    try{
        data->setData(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUData::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de duracao
void TUDuracao::setUp(){
    duracao = new Duracao();
    estado = SUCESSO;
}

void TUDuracao::tearDown(){
    delete duracao;
}

void TUDuracao::testarCenarioSucesso(){
    try{
        duracao->setDuracao(VALOR_VALIDO);
        if (duracao->getDuracao() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUDuracao::testarCenarioFalha(){
    try{
        duracao->setDuracao(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUDuracao::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de vagas
void TUVagas::setUp(){
    vagas = new Vagas();
    estado = SUCESSO;
}

void TUVagas::tearDown(){
    delete vagas;
}

void TUVagas::testarCenarioSucesso(){
    try{
        vagas->setVagas(VALOR_VALIDO);
        if (vagas->getVagas() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUVagas::testarCenarioFalha(){
    try{
        vagas->setVagas(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUVagas::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de preco
void TUPreco::setUp(){
    preco = new Preco();
    estado = SUCESSO;
}

void TUPreco::tearDown(){
    delete preco;
}

void TUPreco::testarCenarioSucesso(){
    try{
        preco->setPreco(VALOR_VALIDO);
        if (preco->getPreco() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUPreco::testarCenarioFalha(){
    try{
        preco->setPreco(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUPreco::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de codigo de reserva
void TUCodigoReserva::setUp(){
    codigo_reserva = new CodigoReserva();
    estado = SUCESSO;
}

void TUCodigoReserva::tearDown(){
    delete codigo_reserva;
}

void TUCodigoReserva::testarCenarioSucesso(){
    try{
        codigo_reserva->setCodigoReserva(VALOR_VALIDO);
        if (codigo_reserva->getCodigoReserva() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUCodigoReserva::testarCenarioFalha(){
    try{
        codigo_reserva->setCodigoReserva(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUCodigoReserva::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de assento
void TUAssento::setUp(){
    assento = new Assento();
    estado = SUCESSO;
}

void TUAssento::tearDown(){
    delete assento;
}

void TUAssento::testarCenarioSucesso(){
    try{
        assento->setAssento(VALOR_VALIDO);
        if (assento->getAssento() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUAssento::testarCenarioFalha(){
    try{
        assento->setAssento(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUAssento::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de bagagem
void TUBagagem::setUp(){
    bagagem = new Bagagem();
    estado = SUCESSO;
}

void TUBagagem::tearDown(){
    delete bagagem;
}

void TUBagagem::testarCenarioSucesso(){
    try{
        bagagem->setBagagem(VALOR_VALIDO);
        if (bagagem->getBagagem() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUBagagem::testarCenarioFalha(){
    try{
        bagagem->setBagagem(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUBagagem::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de codigo de banco
void TUBanco::setUp(){
    banco = new Banco();
    estado = SUCESSO;
}

void TUBanco::tearDown(){
    delete banco;
}

void TUBanco::testarCenarioSucesso(){
    try{
        banco->setBanco(VALOR_VALIDO);
        if (banco->getBanco() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUBanco::testarCenarioFalha(){
    try{
        banco->setBanco(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUBanco::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de numero de agencia
void TUAgencia::setUp(){
    agencia = new Agencia();
    estado = SUCESSO;
}

void TUAgencia::tearDown(){
    delete agencia;
}

void TUAgencia::testarCenarioSucesso(){
    try{
        agencia->setAgencia(VALOR_VALIDO);
        if (agencia->getAgencia() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUAgencia::testarCenarioFalha(){
    try{
        agencia->setAgencia(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUAgencia::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de numero da conta
void TUNumeroConta::setUp(){
    numero = new NumeroConta();
    estado = SUCESSO;
}

void TUNumeroConta::tearDown(){
    delete numero;
}

void TUNumeroConta::testarCenarioSucesso(){
    try{
        numero->setNumeroConta(VALOR_VALIDO);
        if (numero->getNumeroConta() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUNumeroConta::testarCenarioFalha(){
    try{
        numero->setNumeroConta(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUNumeroConta::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de nome
void TUNome::setUp(){
    nome = new Nome();
    estado = SUCESSO;
}

void TUNome::tearDown(){
    delete nome;
}

void TUNome::testarCenarioSucesso(){
    try{
        nome->setNome(VALOR_VALIDO);
        if (nome->getNome() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUNome::testarCenarioFalha(){
    try{
        nome->setNome(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUNome::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de email
void TUEmail::setUp(){
    email = new Email();
    estado = SUCESSO;
}

void TUEmail::tearDown(){
    delete email;
}

void TUEmail::testarCenarioSucesso(){
    try{
        email->setEmail(VALOR_VALIDO);
        if (email->getEmail() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUEmail::testarCenarioFalha(){
    try{
        email->setEmail(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUEmail::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de senha
void TUSenha::setUp(){
    senha = new Senha();
    estado = SUCESSO;
}

void TUSenha::tearDown(){
    delete senha;
}

void TUSenha::testarCenarioSucesso(){
    try{
        senha->setSenha(VALOR_VALIDO);
        if (senha->getSenha() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUSenha::testarCenarioFalha(){
    try{
        senha->setSenha(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUSenha::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de cpf
void TUCpf::setUp(){
    cpf = new Cpf();
    estado = SUCESSO;
}

void TUCpf::tearDown(){
    delete cpf;
}

void TUCpf::testarCenarioSucesso(){
    try{
        cpf->setCpf(VALOR_VALIDO);
        if (cpf->getCpf() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUCpf::testarCenarioFalha(){
    try{
        cpf->setCpf(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUCpf::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}

// Metodos do teste de telefone
void TUTelefone::setUp(){
    telefone = new Telefone();
    estado = SUCESSO;
}

void TUTelefone::tearDown(){
    delete telefone;
}

void TUTelefone::testarCenarioSucesso(){
    try{
        telefone->setTelefone(VALOR_VALIDO);
        if (telefone->getTelefone() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUTelefone::testarCenarioFalha(){
    try{
        telefone->setTelefone(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUTelefone::run(){
    setUp();
    testarCenarioSucesso();
    testarCenarioFalha();
    tearDown();
    return estado;
}
