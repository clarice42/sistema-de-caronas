# Sistema de Caronas 

Trabalho da disciplina Técnicas de Programação 1

Nesse trabalho, foi criada a biblioteca para implementação do sistema de caronas.
Foram desenvolvidas classes de domínio e entidade e todas foram submetidas a testes unitários.
O trabalho foi desenvolvido na linguagem C++ e possui projeto em CodeBlocks.
O compilador utilizado foi o GNU GCC Compiler, com a flag C++14ISO.
